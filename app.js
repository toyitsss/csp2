const express = require('express')

const app = express();

const mongoose = require('mongoose');

const cors = require('cors');

let PORT = 3008;

const router = express.Router()

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
// // const orderRoutes = require('./routes/orderRoutes');

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/csp2?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log ("And We're Connected"));

app.use('/users', userRoutes)
app.use('/products', productRoutes)





app.listen(process.env.PORT || 3001, () => console.log (`Server is currently running at ${PORT}`))