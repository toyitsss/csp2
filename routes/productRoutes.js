const express = require('express');

const router = express.Router();

const productController = require('../controllers/productController')

const auth = require('./../auth');

//get/retrieve all products
router.get('/all', (req, res) => {
	productController.getAll().then(resultFromController => res.send(resultFromController))
})


//get/retrieve all active products
router.get('/active', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})

//add products (admin only)
router.post('/', auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
    if(admin == true){
		productController.addProduct(req.body).then(result => res.send(result));
	}else{
		return res.send ('Not Accessible. For Admin only!');
	}
	
})


//Get Specific Product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params.productId)
	.then(resultFromController => res.send(resultFromController))
})



//Update Product
router.put('/:productId', auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
		if(admin == false){
			res.send (false)
		}else{
			productController.editProductInfo(req.params.productId, req.body)
			.then(result => res.send(result))
	}
})


//Archive Product
//set isActive to false
router.put('/:productId/archive', auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
		if (admin == false){
			res.send (false)
		}else{
			productController.archiveProduct(req.params.productId)
			.then(result => res.send(result))
		}
	})


//Unarchive Product or Activate Product
router.put('/:productId/activate', auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
	if(admin == false){
		res.send (false)
	}else{
		productController.activateProduct(req.params.productId)
		.then(result => res.send(result))
	}
})








module.exports = router;