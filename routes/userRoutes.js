const express = require('express');

const router = express.Router();

const userController = require('../controllers/userControllers')

const auth = require('./../auth');


// registering a user
router.post('/register', (req, res)=>{

	userController.register(req.body)
	.then(resultFromController => res.send(resultFromController))
})


//login a user
router.post('/login', (req, res) => {

	userController.login(req.body)
	.then(resultFromController => res.send(resultFromController))
})


//get all user
router.get('/allUsers', (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
		if(admin == true){
			userController.getAllUsers()
			.then(resultFromController => res.send(resultFromController))
		}else{
			return res.send(false);
		}
})



//get my Orders as a regular user
//Get my orders (regular user)
router.get('/myOrders', auth.verify, (req, res) => {
	const {isAdmin, id} = auth.decode(req.headers.authorization);
		if(isAdmin == true){
			return res.send(false);
		}else{
			userController.getMyOrders(id).then(resultFromController => res.send(resultFromController));
	}
})




router.post("/checkout", auth.verify, (req, res) => {
//if admin is true
	//res.send(false)
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	let userId = auth.decode(req.headers.authorization).id

	if(isAdmin === false){
    
		userController.checkOut(userId, req.body).then(resultFromController => res.send(resultFromController));

    }else{

    	return res.send (false)

    }

})







//userRoutes.js
//View all the orders (Admin)
router.get("/orders", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)

    userController.getAllOrders(userData)
        .then(result => {
            res.send(result)
        });
})




















module.exports = router;