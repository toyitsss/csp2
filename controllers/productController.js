const Product = require('../models/Product');

const auth = require('../auth');

const bcrypt = require ('bcrypt');




//create product (admin only)
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newProduct.save().then((result, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


//get/retrieve all products
module.exports.getAll = () => {
	return Product.find({}).then(result => {
		if(result !== null){
			return result;
		}else{
			return error
		}	
	})
}




//get/retrive all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		if(result){
			return result;
		}else{
			return error
		}
	})
}


//get/retrieve a single product using findById
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
		}else{
			return result;
		}
	})
}



//Update a product
module.exports.editProductInfo = (productId, reqBody) => {
		const {name, description, price} = reqBody
		const updatedProduct = {
			name: name,
			description: description,
			price: price
	}

	return Product.findByIdAndUpdate(productId, updatedProduct, {new:true}).then(result => {console.log(result)
		return result
	})
}



//Archive Product
module.exports.archiveProduct = (params) => {

	return Product.findByIdAndUpdate(params, {isActive: false}).then( (result, error) => {

		if(result == null){
			return `Product not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}




//Unarchive Product
module.exports.activateProduct = (params) => {

	return Product.findByIdAndUpdate(params, {isActive: true}).then( (result, error) => {

		if(result == null){
			return `Product not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}



