
const User = require('../models/User');

const Product = require('../models/Product');

const auth = require('../auth');

const bcrypt = require ('bcrypt');



//registering a user
module.exports.register = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {
		if(result){
			return "The email address you have entered is already registered."
		}else{
			const {email, password} = reqBody

			const newUser = new User ({
				email: email,
				password: bcrypt.hashSync(password, 10)
			})

			return newUser.save().then( (result, error) => {
				if(result){
					return true
				}else{
					return error
				}
			})
		}
	})
	
}



//login a user
module.exports.login = (reqBody) => {
	const {email, password} = reqBody

	return User.findOne({email: email}).then((result, error) => {
		console.log(result);

		if(result == null) {
			console.log('email null');
			return false;
		}else{

			let isPwdCorrect = bcrypt.compareSync(password, result.password)
			if(isPwdCorrect == true){
				return{access: auth.createAccessToken(result)}
			}else{
				return false
			}
		}
	})
}




//get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		if(result !== null){
			return result;
		}else{
			return error;
		}
	})
}



//Get my orders (regular user)
module.exports.getMyOrders = (id) => {
	return User.findById(id).then((result, error) => {
		if(result !== null){
			return result.orders;
		}else{
			return error
		}	
	})
}



//checkout as a regular user
module.exports.checkOut = async (userId, cart) =>{
//check if user exists
	return User.findById(userId).then(user => {
		if(user === null){
			return false
		}else{
			//push the contents of the cart to user

			user.orders.push({
				products: cart.products,
				totalAmount: cart.totalAmount
			})
			return user.save().then((updatedUser, error) => {
				if(error){
					return false
				}else{
					const total = updatedUser.orders.length 
					const lastOrder = updatedUser.orders[total - 1]	

					const find = lastOrder.products[0].productId;
					const push = lastOrder._id;

					return Product.findByIdAndUpdate(find,
						{$addToSet: 
							{orders: [{orderId: push }]}},{new: true})		
				}
			})
		}
	})
}




//View all the orders (Admin)

module.exports.getAllOrders = async (userData) => {
    if (userData.isAdmin == true) {

        const allOrders = await User.find().select("-password -isAdmin -__v")
        let allActiveOrders = []

        for (let i = 0; i < allOrders.length; i++) {
            if (allOrders[i].orders.length >= 1) {
                allActiveOrders.push(allOrders)
            }
        }

        return allActiveOrders

    } return { message: "User is not allowed to get all orders" }
}